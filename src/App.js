import 'babel-polyfill';
import React, { Component } from 'react';
import { Admin, Delete, Resource } from 'admin-on-rest';

import './App.css';

import authClient from './authClient';
import sagas from './sagas';
import themeReducer from './themeReducer';
import Login from './Login';
import Layout from './Layout';
import Menu from './Menu';
import { Dashboard } from './dashboard';
import customRoutes from './routes';
import translations from './i18n';

import { VisitorList, VisitorEdit, VisitorDelete, VisitorIcon } from './visitors';
import { CommandList, CommandEdit, CommandIcon } from './commands';
import { ItemList, ItemCreate, ItemEdit, ItemIcon } from './items';
import { CategoryList, CategoryEdit, CategoryIcon, CategoryCreate } from './categories';
import { PromoList, PromoEdit, PromoIcon, PromoCreate } from './promos';
import restClient from './restClient';


class App extends Component {
    render() {
        return (
            <Admin
                title="Web Service"
                restClient={restClient}
                customReducers={{ theme: themeReducer }}
                customSagas={sagas}
                customRoutes={customRoutes}
                authClient={authClient}
                dashboard={Dashboard}
                loginPage={Login}
                appLayout={Layout}
                // menu={Menu}
                messages={translations}
            >
                <Resource name="users" list={VisitorList} edit={VisitorEdit} remove={VisitorDelete} icon={VisitorIcon} />
                <Resource name="orders" list={CommandList} edit={CommandEdit} remove={Delete} icon={CommandIcon} options={{ label: 'Orders' }}/>
                <Resource name="orderdetail" />
                <Resource name="items" list={ItemList} create={ItemCreate} edit={ItemEdit} remove={Delete} icon={ItemIcon} />
                <Resource name="categories" list={CategoryList} create={CategoryCreate} edit={CategoryEdit} remove={Delete} icon={CategoryIcon} />
                <Resource name="promos" list={PromoList} edit={PromoEdit} create={PromoCreate} remove={Delete} icon={CategoryIcon} />
            </Admin>
        );
    }
}

export default App;
