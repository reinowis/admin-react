import React from 'react';
import {
    translate,
    Datagrid,
    Edit,
    Create,
    EditButton,
    List,
    NumberField,
    DateField,
    ReferenceManyField,
    SimpleForm,
    TextField,
    TextInput,
    TabbedForm,
    FormTab,
    DateTimeFormat,
    DateInput,
    NumberInput,
    CheckboxGroupInput,
    ReferenceArrayInput,
    SelectArrayInput,
    ReferenceArrayField,
    SelectArrayField,
    Filter
} from 'admin-on-rest';
import Icon from 'material-ui/svg-icons/action/bookmark';

import ThumbnailField from '../items/ThumbnailField';
import ItemRefField from '../items/ItemRefField';
import LinkToRelatedItems from './LinkToRelatedItems';
import TimeInput from '../input/TimeInput';
export const PromoIcon = Icon;
export const date = new Date(Date.UTC(2013, 1, 1, 14, 0, 0));  

export const dateParser = v => {
  const regexp = /(\d{4})-(\d{2})-(\d{2})/
  var match = regexp.exec(v);
  if (match === null) return;
  var year = match[1];
  var month = match[2];
  var day = match[3];
  const d = [year, month, day].join("-");
  return d;
};
export const timeParser = v => {
    const regexp = /(\d{2}):(\d{2}):(\d{2})/
    var match = regexp.exec(v);
    if (match === null) return;
    var hours = match[1];
    var minutes = match[2];
    var seconds = match[3];
    const d = [hours, minutes, seconds].join(":");
    console.log(d);
    return d;
  };
export const datesFormatter = v => {
    const regexp = /\d/g
    var dates = [];
    // if (v instanceof String)
    if (v != undefined)
        dates = v.toString().match(regexp);
    return dates;
};  
export const datesParser = v => {
    var dates = "";
    if (v instanceof Array)
        for (const i of v)
        {
            dates += i.toString();
        }     
    return dates;
};  

export const dates_choices = [
    { id: 0, day:"Monday" },
    { id: 1, day:"Tuesday" },
    { id: 2, day:"Wednesday" },
    { id: 3, day:"Thursday" },
    { id: 4, day:"Friday" },
    { id: 5, day:"Saturday" },
    { id: 6, day:"Sunday" },
];
const DayField = ({ record }) => <span>{record.day}</span>;
export const PromoFilter = props => (
    <Filter {...props}>
        <TextInput label="pos.search" source="name" alwaysOn />
        <DateInput source="date_start__gte" label="Start date greater than" parse={dateParser}/>
        <DateInput source="date_end__lte" label="End date less than" parse={dateParser}/>
        <TimeInput source="Time_start__gte" label="Start time greater than " parse={timeParser}/>
        <TimeInput source="Time_end__lte" label="End time less than " parse={timeParser}/>
        <CheckboxGroupInput source="dates__icontains" choices={dates_choices} optionText={<DayField />} format={datesFormatter} parse={datesParser}/>
        {/* <QuickFilter label="resources.items.fields.stock_lte" source="stock_lte" defaultValue={10} /> */}
    </Filter>
);
export const PromoList = (props) => (
    <List {...props} sort={{ field: 'name', order: 'ASC' }} filters={<PromoFilter/>}>
        <Datagrid >
            <TextField source="name" style={{ padding: '0 12px 0 25px' }} />
            <TextField source="date_start" parse={dateParser} label="Start date"/>
            <TextField source="date_end" parse={dateParser} label="End date"/>
            <TextField source="time_start" parse={timeParser} label="Start time"/>
            <TextField source="time_end" parse={timeParser} label="End time"/>
            <LinkToRelatedItems/>
            <EditButton />
        </Datagrid>
    </List>
);
const validatePromos = (values) => {
    const errors = {};
    if (!values.name) {
        errors.name = ['Name is required'];
    }
    if (!values.value) {
        errors.value = ['Discount value is required'];
    } else if (values.value < 0) {
        errors.value = ['Discount value must be bigger than 0'];
    }
    if (!values.date_start)
        errors.date_start = ['Start date is required'];
    if (!values.date_end)
        errors.date_end = ['End date is required'];
    if (!values.time_start)
        errors.time_start = ['Start time is required'];    
    if (!values.time_end)
        errors.time_end = ['End time is required'];   
    return errors
};
const PromoTitle = translate(({ record, translate }) => <span>{translate('resources.promos.name', { smart_count: 1 })} "{record.name}"</span>);
export const PromoCreate = (props) => (
    <Create {...props} >
        <TabbedForm validate={validatePromos}>
            <FormTab label="Info">
            <TextInput source="name"/>
            <NumberInput source="value" label="Discount value" options={{
                min: 0,
                max: 100,
            }} />
            <DateInput source="date_start" parse={dateParser} label="Start date"/>
            <DateInput source="date_end" parse={dateParser} label="End date"/>
            <CheckboxGroupInput source="dates" choices={dates_choices} optionText={<DayField />} format={datesFormatter} parse={datesParser}/>
            <TimeInput source="time_start" parse={timeParser} label="Start time"/>
            <TimeInput source="time_end" parse={timeParser} label="End time"/>
            </FormTab>
            <FormTab label="Items">
            <ReferenceArrayInput source="promo_items" reference="items" label="List of items" allowEmpty>
                <SelectArrayInput optionText="name" />
            </ReferenceArrayInput>
            </FormTab>
        </TabbedForm>
    </Create>
);

export const PromoEdit = (props) => (
    <Edit title={<PromoTitle />} {...props}>
        <TabbedForm>
            <FormTab label="Info">
            <TextInput source="name" />
            <NumberInput source="value" label="Discount value" options={{
                min: 0,
                max: 100,
            }} />
            <DateInput source="date_start" parse={dateParser}/>
            <DateInput source="date_end" parse={dateParser}/>
            <CheckboxGroupInput source="dates" choices={dates_choices} optionText={<DayField />} format={datesFormatter} parse={datesParser} allowEmpty/>
            <TimeInput source="time_start" parse={timeParser} label="Time Start"/>
            <TimeInput source="time_end" parse={timeParser} label="Time End"/>
            </FormTab>
            <FormTab label="Items">
            <ReferenceArrayInput source="promo_items" reference="items" label="List of items" allowEmpty>
                <SelectArrayInput optionText="name" />
            </ReferenceArrayInput>
            <ReferenceArrayField source="promo_items" reference="items" label="List of items" allowEmpty>
                <Datagrid>
                    <TextField source="id"/>
                    <TextField source="name"/>
                    <TextField source="name"/>
                    <TextField source="quantity"/>
                    <EditButton />
                </Datagrid>
            </ReferenceArrayField>
            </FormTab>
        </TabbedForm>
    </Edit>
);
