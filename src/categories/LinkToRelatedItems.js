import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router-dom';
import { translate } from 'admin-on-rest';
import { stringify } from 'query-string';

import { ItemIcon } from '../items';

const LinkToRelatedItems = ({ record, translate }) => (
    <FlatButton
        primary
        label="Items"
        icon={<ItemIcon />}
        containerElement={<Link
            to={{
                pathname: '/items',
                search: stringify({ page: 1, perPage: 25, filter: JSON.stringify({ categories: record.id }) }),
            }}
        />}
    />
);

export default translate(LinkToRelatedItems);
