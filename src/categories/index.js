import React from 'react';
import {
    translate,
    Datagrid,
    Edit,
    Create,
    EditButton,
    List,
    NumberField,
    ReferenceField,
    ReferenceManyField,
    ReferenceArrayField,
    SimpleForm,
    TextField,
    TextInput,
    ReferenceArrayInput,
    SelectArrayInput,
    ReferenceInput,
    SelectInput,
    CheckboxGroupInput
} from 'admin-on-rest';
import Icon from 'material-ui/svg-icons/action/bookmark';

import ThumbnailField from '../items/ThumbnailField';
import ItemRefField from '../items/ItemRefField';
import LinkToRelatedItems from './LinkToRelatedItems';
import ParentInput from '../input/ParentInput';
export const CategoryIcon = Icon;
export const cateFormatter = v => {
    console.log(v);
    if (v=="0"||v=="null")
        return "";
};
export const CategoryList = (props) => (
    <List {...props} sort={{ field: 'name', order: 'ASC' }}>
        <Datagrid >
            <TextField source="name" style={{ padding: '0 12px 0 25px' }} />
            <ReferenceField label="Parent" source="parent" reference="categories" format={cateFormatter} allowEmpty>
                <TextField source="name" />
            </ReferenceField>
            <LinkToRelatedItems label="Items"/>
            <EditButton />
        </Datagrid>
    </List>
);

const CategoryTitle = translate(({ record, translate }) => <span>{translate('resources.categories.name', { smart_count: 1 })} "{record.name}"</span>);
// export const parentList = 
export const CategoryEdit = (props) => {
    
    return (
    <Edit title={<CategoryTitle />} {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <ReferenceInput label="Parent" source="parent" reference="categories" filter="" allowEmpty>
                <SelectInput optionText="name" allowEmpty="True"/>
            </ReferenceInput>
            <ReferenceArrayInput source="cate_items" reference="items" label="List of items" allowEmpty>
                <SelectArrayInput optionText="name" />
            </ReferenceArrayInput>
            <ReferenceArrayField reference="items" source="cate_items" label="List of items" perPage={5} allowEmpty>
                <Datagrid>
                    <TextField source="id"/>
                    <TextField source="name" label="Name"/>
                    <NumberField source="price" options={{ style: 'currency', currency: 'USD' }} />
                    <NumberField source="quantity" />
                    <EditButton />
                </Datagrid>
            </ReferenceArrayField>
        </SimpleForm>
    </Edit>
    );
};
export const CategoryCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <ReferenceInput source="parent" reference="categories" label="Parent" allowEmpty>
                <SelectInput optionText="name" allowEmpty={true}/>
            </ReferenceInput>
            <ReferenceArrayInput source="cate_items_id" reference="items" label="List of items" allowEmpty>
                <SelectArrayInput optionText="name" allowEmpty={true}/>
            </ReferenceArrayInput>
        </SimpleForm>
    </Create>
);
