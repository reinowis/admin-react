import React from 'react';
import { Card, CardTitle } from 'material-ui/Card';
import { List, ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import { translate } from 'admin-on-rest';

const style = { flex: 1 };

export default translate(({ orders = [], customers = {}, translate }) => (
    <Card style={style}>
        <CardTitle title={translate('pos.dashboard.pending_orders')} />
        <List>
            {orders.map(record =>
                <ListItem
                    key={record.id}
                    href={`#/orders/${record.id}`}
                    primaryText={new Date(record.date).toLocaleString('en-GB')}
                    secondaryText={
                        <p>
                            {translate('pos.dashboard.orders.items', {
                                user_name: customers[record.customer] ? `${customers[record.customer].first_name} ${customers[record.customer].last_name}` : ''
                            })}
                        </p>
                    }
                    rightAvatar={<strong>{record.price}$</strong>}
                />
            )}
        </List>
    </Card>
));
