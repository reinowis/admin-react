import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TimePicker from 'material-ui/TimePicker';
// import FieldTitle from 'admin-on-rest/src/util/FieldTitle';
import FieldTitle from 'admin-on-rest';
export const datify = input => {
    if (!input) {
        return null;
    }
    if (input instanceof Date)
        var time = input.toString();
    else{
        var time = new Date();
    }
        input = input.match(/(\d\d)(?::(\d\d))(?::(\d\d))/);
        time.setHours(input[1]);
        time.setMinutes(input[2]);
        time.setSeconds(input[3]);    
    if (isNaN(time)) {
        throw new Error(`Invalid date: ${time}`);
    }
    return time;
};

class TimeInput extends Component {
    onChange = (_, date) => {
        this.props.input.onChange(date);
        this.props.input.onBlur();
    };

    /**
     * This aims to fix a bug created by the conjunction of
     * redux-form, which expects onBlur to be triggered after onChange, and
     * material-ui, which triggers onBlur on <TimePicker> when the user clicks
     * on the input to bring the focus on the calendar rather than the input.
     *
     * @see https://github.com/erikras/redux-form/issues/1218#issuecomment-229072652
     */
    onBlur = () => {};

    onDismiss = () => this.props.input.onBlur();

    render() {
        const {
            input,
            isRequired,
            label,
            meta,
            options,
            source,
            elStyle,
            resource,
        } = this.props;
        if (typeof meta === 'undefined') {
            throw new Error(
                "The TimeInput component wasn't called within a redux-form <Field>. Did you decorate it and forget to add the addField prop to your component? See https://marmelab.com/admin-on-rest/Inputs.html#writing-your-own-input-component for details."
            );
        }
        const { touched, error } = meta;

        return (
            <TimePicker
                {...input}
                errorText={touched && error}
                format="24hr"
                container="inline"
                autoOk
                value={datify(input.value)}
                floatingLabelText={label}
                onChange={this.onChange}
                onBlur={this.onBlur}
                onDismiss={this.onDismiss}
                style={elStyle}
                {...options}
            />
        );
    }
}

TimeInput.propTypes = {
    addField: PropTypes.bool.isRequired,
    elStyle: PropTypes.object,
    input: PropTypes.object,
    isRequired: PropTypes.bool,
    label: PropTypes.string,
    meta: PropTypes.object,
    options: PropTypes.object,
    resource: PropTypes.string,
    source: PropTypes.string,
    format: PropTypes.string
};

TimeInput.defaultProps = {
    addField: true,
    options: {},
    floatingLabelText: ""
};

export default TimeInput;
