import React from 'react';
import { Link } from 'react-router-dom';

const ItemRefField = ({ record, basePath }) =>
    <Link to={`items/${record.id}`}>{record.reference}</Link>;

ItemRefField.defaultProps = {
    source: 'id',
    label: 'Reference',
};

export default ItemRefField;
