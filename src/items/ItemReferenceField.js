import React from 'react';
import { ReferenceField, TextField } from 'admin-on-rest';

const ItemReferenceField = (props) => (
    <ReferenceField label="Item" source="Item_id" reference="Items" {...props}>
        <TextField source="reference" />
    </ReferenceField>
)
ItemReferenceField.defaultProps = {
    source: 'id',
    addLabel: true,
};

export default ItemReferenceField;
