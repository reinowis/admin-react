import React from 'react';
import {
    translate,
    Create,
    Datagrid,
    DateField,
    Edit,
    EditButton,
    Filter,
    FormTab,
    List,
    NumberInput,
    ReferenceInput,
    ReferenceArrayInput,
    ReferenceManyField,
    ReferenceArrayField,
    SelectInput,
    SelectArrayInput,
    TabbedForm,
    TextField,
    TextInput,
    SingleFieldList,
    ChipField,
    ImageInput,
    ImageField,
    NumberField,
} from 'admin-on-rest';
import Icon from 'material-ui/svg-icons/image/collections';
import Chip from 'material-ui/Chip';
import RichTextInput from 'aor-rich-text-input';

// import CustomerReferenceField from '../visitors/CustomerReferenceField';
import Poster from './Poster';

export const ItemIcon = Icon;

const QuickFilter = translate(({ label, translate }) => <Chip>{translate(label)}</Chip>);

export const ItemFilter = props => (
    <Filter {...props}>
        <TextInput label="pos.search" source="name" alwaysOn />
        <ReferenceInput source="categories" reference="categories">
            <SelectInput source="name" />
        </ReferenceInput>
        <NumberInput source="price__gte" label="Price greater than"/>
        <NumberInput source="price__lte" label="Price less than"/>
        <NumberInput source="quantity__gte" label="Quantity greater than "/>
        <NumberInput source="quantity__lte" label="Quantity less than "/>
        {/* <QuickFilter label="resources.items.fields.stock_lte" source="stock_lte" defaultValue={10} /> */}
    </Filter>
);

export const ItemList = props => (
        <List {...props} filters={<ItemFilter />}>
            <Datagrid>
            <TextField source="id" />        
            <TextField source="name" />    
            <ReferenceArrayField reference="categories" source="categories">
                <SingleFieldList>
                    <ChipField source="name" />
                </SingleFieldList>
            </ReferenceArrayField>
            <NumberField source="price" />
            <NumberField source="quantity" />
            <TextField source="info" />
            <EditButton></EditButton>
            </Datagrid>
    </List>
);

export const ItemCreate = (props) => (
    <Create {...props}>
        <TabbedForm>
            {/* <FormTab label="resources.items.tabs.image">
                <TextInput source="images" options={{ fullWidth: true }} validation={{ required: true }} />
                <TextInput source="thumbnail" options={{ fullWidth: true }} validation={{ required: true }} />
                <ImageInput source="images" label="Related pictures" accept="image/*">
                    <ImageField source="src" title="title" />
                </ImageInput>
            </FormTab> */}
            <FormTab label="resources.items.tabs.details">
                <TextInput source="name"/>
                <NumberInput source="price" validation={{ required: true }} elStyle={{ width: '5em' }} />
                <ReferenceArrayInput source="categories" reference="categories" allowEmpty>
                    <SelectArrayInput source="name" />
                </ReferenceArrayInput>
                <ReferenceArrayInput source="promos" reference="promos" label="Promos"  allowEmpty>
                    <SelectArrayInput optionText="name" />
                </ReferenceArrayInput>
                <NumberInput source="quantity" validation={{ required: true }} elStyle={{ width: '5em' }} />
            </FormTab>
            <FormTab label="resources.items.tabs.description">
                <RichTextInput source="info" addLabel={false}/>
            </FormTab>
        </TabbedForm>
    </Create>
);

const ItemTitle = ({ record }) => <span>Poster #{record.id}: {record.name}</span>;
export const ItemEdit = (props) => (
    <Edit {...props} title={<ItemTitle />}>
        <TabbedForm>
            {/* <FormTab label="resources.items.tabs.image">
                <Poster />
                <ImageInput source="images" label="Related pictures" accept="image/*">
                    <ImageField source="src" title="title" />
                </ImageInput>
            </FormTab> */}
            <FormTab label="resources.items.tabs.details">
            <TextInput source="name"/>
                <NumberInput source="price" elStyle={{ width: '5em' }} />
                <NumberInput source="quantity" elStyle={{ width: '5em' }} />
                <ReferenceArrayInput source="categories" reference="categories"  allowEmpty>
                    <SelectArrayInput optionText="name" />
                </ReferenceArrayInput>
                <ReferenceArrayInput source="promos" reference="promos" label="Promos"  allowEmpty>
                    <SelectArrayInput optionText="name" />
                </ReferenceArrayInput>
            </FormTab>
            <FormTab label="resources.items.tabs.description">
                <RichTextInput source="info" addLabel={false}/>
            </FormTab>
        </TabbedForm>
    </Edit>
);
