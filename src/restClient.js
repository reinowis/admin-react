import { fetchUtils, Admin, Resource } from 'admin-on-rest';
import { stringify } from 'query-string';
import addUploadFeature from './addUploadFeature';
// import { fetchJson } from 'admin-on-rest/src/util/fetch';
import {
    GET_LIST,
    GET_ONE,
    GET_MANY,
    GET_MANY_REFERENCE,
    CREATE,
    UPDATE,
    DELETE,
} from 'admin-on-rest/src/rest/types';
const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    // add your own headers here
    if (localStorage.getItem('Token'))
    {
        var token = localStorage.getItem('Token');
        options.headers.set('Authorization',"Token "+token );
    }
    // options.headers.set('mode','no-cors');
    return fetchUtils.fetchJson(url, options);
}


/**
 * Maps admin-on-rest queries to a simple REST API
 *
 * The REST dialect is similar to the one of FakeRest
 * @see https://github.com/marmelab/FakeRest
 * @example
 * GET_LIST     => GET http://my.api.url/posts?sort=['title','ASC']&range=[0, 24]
 * GET_ONE      => GET http://my.api.url/posts/123
 * GET_MANY     => GET http://my.api.url/posts?filter={ids:[123,456,789]}
 * UPDATE       => PUT http://my.api.url/posts/123
 * CREATE       => POST http://my.api.url/posts/123
 * DELETE       => DELETE http://my.api.url/posts/123
 */
const simpleRestClient = (apiUrl, httpClient = fetchUtils.fetchJson) => {
    /**
     * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
     * @param {String} resource Name of the resource to fetch, e.g. 'posts'
     * @param {Object} params The REST request params, depending on the type
     * @returns {Object} { url, options } The HTTP request parameters
     */
    const convertRESTRequestToHTTP = (type, resource, params) => {
        let url = '';
        const options = {};
        switch (type) {
            case GET_LIST: {
                const { page, perPage } = params.pagination;
                const { field, order } = params.sort;
                var order_field = field;
                if (order == "DESC")
                    order_field = "-"+field;
                var filter = "";
                Object.keys(params.filter).map((param) => {filter += "&"+param + "=" + params.filter[param]});
                url = `${apiUrl}/${resource}?page=${page}&ordering=${order_field}${filter}`;
                break;
            }
            case GET_ONE:
                url = `${apiUrl}/${resource}/${params.id}`;
                break;
            case GET_MANY: {
                var query = ""
                if (params.ids != "")
                    query = `?id=${params.ids}`;
                url = `${apiUrl}/${resource}${query}`;
                break;
            }
            case GET_MANY_REFERENCE: {
                const { page, perPage } = params.pagination;
                const { field, order } = params.sort;
                const query = {
                    sort: JSON.stringify([field, order]),
                    page: JSON.stringify([
                        (page - 1) * perPage,
                        page * perPage - 1,
                    ]),
                    filter: JSON.stringify({
                        ...params.filter,
                        [params.target]: params.id,
                    }),
                };
                var order_field = field;
                if (order == "DESC")
                    var order_field = "-"+field;
                var filter = "";
                Object.keys(params.filter).map((param) => {filter += "&"+param + "=" + params.filter[param]});
                const search = [params.target]+"="+params.id;
                url = `${apiUrl}/${resource}?page=${page}&ordering=${order_field}${filter}`;
                break;
            }
            case UPDATE:
                url = `${apiUrl}/${resource}/${params.id}/`;
                options.method = 'PATCH';
                if (resource=='promos')
                {
                    if (params.data["promo_items"])
                        params.data["promo_items_id"] = params.data["promo_items"];
                }
                if (resource=='categories')
                {
                    if (params.data["cate_items"])
                        params.data["cate_items_id"] = params.data["cate_items"];
                }
                options.body = JSON.stringify(params.data);
                break;
            case CREATE:
                url = `${apiUrl}/${resource}/`;
                options.method = 'POST';
                if (resource=='promos')
                {
                    if (params.data["promo_items"])
                        params.data["promo_items_id"] = params.data["promo_items"];
                }
                if (resource=='categories')
                {
                    if (params.data["cate_items"])
                        params.data["cate_items_id"] = params.data["cate_items"];
                }
                options.body = JSON.stringify(params.data);
                // console.log(options.body);
                break;
            case DELETE:
                url = `${apiUrl}/${resource}/${params.id}`;
                options.method = 'DELETE';
                break;
            default:
                throw new Error(`Unsupported fetch action type ${type}`);
        }
        return { url, options };
    };

    /**
     * @param {Object} response HTTP response from fetch()
     * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
     * @param {String} resource Name of the resource to fetch, e.g. 'posts'
     * @param {Object} params The REST request params, depending on the type
     * @returns {Object} REST response
     */
    const convertHTTPResponseToREST = (response, type, resource, params) => {
        const { headers, json } = response;
        switch (type) {
            case GET_LIST:
            case GET_MANY_REFERENCE:
                if (!headers.has('content-range')) {
                    throw new Error(
                        'The Content-Range header is missing in the HTTP Response. The simple REST client expects responses for lists of resources to contain this header with the total number of results to build the pagination. If you are using CORS, did you declare Content-Range in the Access-Control-Expose-Headers header?'
                    );
                }
                return {
                    data: json,
                    total: parseInt(
                        headers
                            .get('content-range')
                            .split('/')
                            .pop(),
                        10
                    ),
                };
            case CREATE:
                return { data: { ...params.data, id: json.id } };
            default:
                return { data: json };
        }
    };

    /**
     * @param {string} type Request type, e.g GET_LIST
     * @param {string} resource Resource name, e.g. "posts"
     * @param {Object} payload Request parameters. Depends on the request type
     * @returns {Promise} the Promise for a REST response
     */
    return (type, resource, params) => {
        const { url, options } = convertRESTRequestToHTTP(
            type,
            resource,
            params
        );
        return httpClient(url, options).then(response =>
            convertHTTPResponseToREST(response, type, resource, params)
        );
    };
};

const restClient = simpleRestClient('http://localhost:8000', httpClient);

const uploadCapableClient = addUploadFeature(restClient);
// export default (type, resource, params) => new Promise(resolve => setTimeout(() => resolve(restClient), 500));
export default (type, resource, params) => new Promise(resolve => setTimeout(() => resolve(restClient(type, resource, params)), 500));
