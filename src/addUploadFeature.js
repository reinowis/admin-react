/**
* Convert a `File` object returned by the upload input into
* a base 64 string. That's easier to use on FakeRest, used on
* the ng-admin example. But that's probably not the most optimized
* way to do in a production database.
*/
const convertFileToBase64 = file => new Promise((resolve, reject) => {
   const reader = new FileReader();
   reader.readAsDataURL(file);

   reader.onload = () => resolve(reader.result);
   reader.onerror = reject;
});

/**
* For posts update only, convert uploaded image in base 64 and attach it to
* the `picture` sent property, with `src` and `title` attributes.
*/
const addUploadCapabilities = requestHandler => (type, resource, params) => {
    if ((type === 'UPDATE'||type === 'CREATE') && resource === 'items') {
        console.log(params.data);
        if (params.data.images && params.data.images.length) {
        console.log("checked");   
        // only freshly dropped images are instance of File
           const formerimages = params.data.images.filter(p => !(p instanceof File));
           const newimages = params.data.images.filter(p => p instanceof File);
            console.log(newimages);
            console.log(formerimages);
           return Promise.all(newimages.map(convertFileToBase64))
               .then(base64images => base64images.map(picture64 => ({
                   src: picture64,
                   title: `${params.data.title}`,
               })))
               .then(transformedNewimages => requestHandler(type, resource, {
                   ...params,
                   data: {
                       ...params.data,
                       images: [...transformedNewimages, ...formerimages],
                   },
               }));
       }
   }

   return requestHandler(type, resource, params);
};
export default addUploadCapabilities;