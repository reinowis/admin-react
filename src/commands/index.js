import React from 'react';
import {
    translate,
    AutocompleteInput,
    BooleanField,
    BooleanInput,
    Datagrid,
    DateField,
    DateInput,
    Edit,
    EditButton,
    Filter,
    List,
    NullableBooleanInput,
    NumberField,
    ReferenceInput,
    ReferenceField,
    ReferenceArrayField,
    ReferenceManyField,
    SelectInput,
    SimpleForm,
    TextField,
    TextInput,
    FormTab,
    TabbedForm,
    SingleFieldList,
    ChipField,
} from 'admin-on-rest';
import Icon from 'material-ui/svg-icons/editor/attach-money';

import NbItemsField from './NbItemsField';
import CustomerReferenceField from '../visitors/CustomerReferenceField';
import LinkToRelatedItems from '../categories/LinkToRelatedItems';
export const CommandIcon = Icon;
export const StatusTextField = ({ record = {} }) => {
    console.log(record.status);
    switch (record.status)
    {
        case -1: return "Cancelled";
        break;
        case 0: return "Pending";
        break;
        case 1: return "Delivered";
        break;
        case 2: return "Received";
        break;
        default: return "";
    }
};
StatusTextField.defaultProps = { label: 'Status' };
const CommandFilter = (props) => (
    <Filter {...props}>
        <TextInput label="pos.search" source="name" alwaysOn />
        <ReferenceInput source="customer" reference="users">
            <AutocompleteInput optionText={choice => `${choice.first_name} ${choice.last_name}`} />
        </ReferenceInput>
        <SelectInput source="status" choices={[
            { id: '-1', name: 'cancelled' },
            { id: '0', name: 'pending' },
            { id: '1', name: 'delivered' },
            { id: '2', name: 'received' },
        ]} />
        <DateInput source="date_gte" />
        <DateInput source="date_lte" />
        <TextInput source="total_gte" />
        <NullableBooleanInput source="returned" />
    </Filter>
);

export const CommandList = (props) => (
    <List {...props} filters={<CommandFilter />} sort={{ field: 'date', order: 'DESC' }} perPage={25}>
        <Datagrid >
            <DateField source="date" showTime />
            <CustomerReferenceField />
            <NumberField source="price" options={{ style: 'currency', currency: 'VND' }} elStyle={{ fontWeight: 'bold' }}/>
            <StatusTextField source="status" />
            <EditButton />
        </Datagrid>
    </List>
);   

const CommandTitle = translate(({ record, translate }) => <span>{translate('resources.orders.name', { smart_count: 1 })} #{record.id}</span>);

export const CommandEdit = (rest) => {
    console.log(rest);

    return (
    <Edit title={<CommandTitle />} {...rest}>
       
            <TabbedForm>
                <FormTab label="Manage">
                {/* <SimpleForm> */}
                <TextField source="phone"/>
                <TextField source="address"/>
                <DateField source="date" />
                <CustomerReferenceField />
                <SelectInput source="status" choices={[
                    { id: -1, name: 'cancelled' },
                    { id: 0, name: 'pending' },
                    { id: 1, name: 'delivered' },
                    { id: 2, name: 'received' },
                ]}/>
                <div style={{ clear: 'both' }} />
                {/* </SimpleForm> */}
                </FormTab>
                <FormTab label="DETAILS">
                    <ReferenceManyField label="Details" reference="orderdetail" target="orders">
                        <Datagrid>
                            <TextField source="id"/>
                            <ReferenceField label="Item" source="item" reference="items">
                                <TextField source="name" />
                            </ReferenceField>
                            <NumberField source="quantity"/>
                            <TextField source="price"/>
                            <TextField source="total"/>
                        </Datagrid>
                    </ReferenceManyField>
                </FormTab>
        </TabbedForm>
        
    </Edit>
)};
