import React from 'react';
import { ReferenceField } from 'admin-on-rest';

import FullNameField from './FullNameField';

const CustomerReferenceField = (props) => (
    <ReferenceField source="customer" reference="users" {...props}>
        <FullNameField />
    </ReferenceField>
);
CustomerReferenceField.defaultProps = {
    source: 'customer',
    addLabel: true,
};

export default CustomerReferenceField;
