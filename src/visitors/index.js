import React from 'react';
import {
    translate,
    BooleanField,
    Datagrid,
    DateField,
    DateInput,
    Delete,
    Edit,
    Filter,
    FormTab,
    List,
    LongTextInput,
    NullableBooleanInput,
    NumberField,
    ReferenceManyField,
    TabbedForm,
    TextField,
    TextInput,
} from 'admin-on-rest';
import Icon from 'material-ui/svg-icons/social/person';

import EditButton from '../buttons/EditButton';
import NbItemsField from '../commands/NbItemsField';
import ItemReferenceField from '../items/ItemReferenceField';
import FullNameField from './FullNameField';
import SegmentsField from './SegmentsField';
import SegmentInput from './SegmentInput';
import SegmentsInput from './SegmentsInput';

export const VisitorIcon = Icon;

const VisitorFilter = (props) => (
    <Filter {...props}>
        <TextInput label="pos.search" source="q" alwaysOn />
        <DateInput source="last_seen_gte" />
        <NullableBooleanInput source="has_ordered" />
        <NullableBooleanInput source="has_newsletter" defaultValue={true} />
        <SegmentInput />
    </Filter>
);

const colored = WrappedComponent => props => props.record[props.source] > 500 ?
    <span style={{ color: 'red' }}><WrappedComponent {...props} /></span> :
    <WrappedComponent {...props} />;

const ColoredNumberField = colored(NumberField);
ColoredNumberField.defaultProps = NumberField.defaultProps;

export const VisitorList = (props) => (
    <List {...props} filters={<VisitorFilter />} sort={{ field: 'last_seen', order: 'DESC' }} perPage={25}>
        <Datagrid bodyOptions={{ stripedRows: true, showRowHover: true }}>
            <TextField source="username" />
            <TextField source="first_name" />
            <TextField source="last_name" />
            <DateField source="last_login" type="datetime" showTime/>
            <EditButton />
        </Datagrid>
    </List>
);

const VisitorTitle = ({ record }) => record ? <FullNameField record={record} size={32} /> : null;

const StatusTextField = ({ record = {} }) => {
    switch (record.status)
    {
        case -1: return "Cancelled";
        case 0: return "Processing";
        case 1: return "Delivered";
        case 2: return "Received";
    }
};
FullNameField.defaultProps = { label: 'Name' };

export const VisitorEdit = (props) => (
    <Edit title={<VisitorTitle />} {...props}>
        <TabbedForm>
            <FormTab label="resources.users.tabs.identity">
                <TextField source="username"/>
                <TextField source="first_name"/>
                <TextField source="last_name"/>
                <TextField type="email" source="email" validation={{ email: true }} options={{ fullWidth: true }} style={{ width: 544 }} />
            </FormTab>
            <FormTab label="resources.users.tabs.orders">
                <ReferenceManyField addLabel={false} reference="orders" target="customer">
                    <Datagrid>
                        <TextField source="id" />
                        <DateField source="date" />
                        <NumberField source="price" options={{ style: 'currency', currency: 'VND' }} />
                        <StatusTextField source="status" />
                        <EditButton />
                    </Datagrid>
                </ReferenceManyField>
            </FormTab>
        </TabbedForm>
    </Edit>
);

const VisitorDeleteTitle = translate(({ record, translate }) => <span>
    {translate('resources.users.page.delete')}&nbsp;
    {record && <img src={`${record.avatar}?size=25x25`} width="25" alt="" />}
    {record && `${record.first_name} ${record.last_name}`}
</span>);

export const VisitorDelete = (props) => <Delete {...props} title={<VisitorDeleteTitle />} />;
